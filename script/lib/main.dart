import 'package:objd/core.dart';

void main(List<String> args){
    createProject(
        Project(
            name : "wpack",
            target : "../../../",
            generate : CustomWidget() 
        ),
        args
    );
}

class CustomWidget extends Widget {
    @override
    Widget generate(Context context){
    	List<Widget> loadList = [];
    	List<Widget> mainList = [];
    	List<Widget> getList = [];

        loadList.add(
            Summon(
                EntityType.area_effect_cloud,
                location : Location.here(),
                tags : ["num_gen"]));
        loadList.add(
            Tellraw(
                Entity.All(),
                show : [
                    TextComponent(
                        "WPack@Console> Reloaded &"),
                    TextComponent.entityNbt(
                        Entity(
                            tags : [
                                "num_gen"
                            ],
                            type : EntityType.area_effect_cloud),
                        path : "UUIDLeast")
                ]));

        mainList.add(
        	Execute(as : Entity.All(),
        		children : [
        			Execute.at(Entity.Selected(),
        				children : [
        					Command()
        				])
        		]));

        getList.add(
        	Give(
        		Entity.Selected(),
        		item : Item(
        			"minecraft:carrot_on_a_stick",
        			nbt : {"WData" : {"type" : 0}, "Unbreakable" : 1}
        			)));

        return Pack(
            name : "wpack",
            main : File("main",
            	child : For.of(mainList)
            	),
            load : File("load",
            	child : For.of(loadList)),
            files : [
	            File("get_item",
	            	child : For.of(getList)),
	            File("fire/type_0")
            ]
        );
    }
}